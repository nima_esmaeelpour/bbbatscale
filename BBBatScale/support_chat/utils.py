from django.conf import settings

from core.models import User


def has_supporter_privileges(user: User) -> bool:
    return user.groups.filter(name=settings.SUPPORTERS_GROUP).exists() or user.is_staff or user.is_superuser
