#!/bin/bash

URL=$CI_ENVIRONMENT_URL
TOTAL_TRIES=10

count=1
total_time=0

export LC_NUMERIC="en_US.UTF-8"
echo "> $URL"
# Calculate average time
while [ $count -le $TOTAL_TRIES ]; do
  result=$(curl -o /dev/null -s -w %{time_total} $URL)
  echo "    Time: $result"

  var=$(echo $result | awk -F":" '{print $1}')

  total_time=$(echo "$total_time + $var" | bc)
  count=$((count + 1))
done

total_time=$(echo "scale=2; $total_time / $TOTAL_TRIES" | bc)
echo "    Average time taken: $total_time"

# Print helpers
if [ -t 1 ]; then
  ncolors=$(tput colors)
  if test -n "$ncolors" && test $ncolors -ge 8; then
    bold="$(tput bold)"
    normal="$(tput sgr0)"
    red="$(tput setaf 1)"
    redbg="$(tput setab 1)"
    green="$(tput setaf 2)"
    greenbg="$(tput setab 2)"
  fi
fi

# Report
if (($(echo "$total_time < 2" | bc -l))); then
  echo "    [ ${green}${bold}OK${normal} ] Average time taken is less than 2 seconds"
else
  echo "    [${red}${bold}FAIL${normal}] Average time taken is greater than 2 seconds"
  exit 1
fi
echo "Smoke Tests Successfully Completed."
