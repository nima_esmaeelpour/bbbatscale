#!/bin/bash

. smoke.sh

TIME_OUT=300
TIME_OUT_COUNT=0

while true
do
  STATUS=$(curl -s -o /dev/null -w '%{http_code}' $CI_ENVIRONMENT_URL)
  if [ $STATUS -eq 200 ]; then
    smoke_url_ok $CI_ENVIRONMENT_URL
    smoke_assert_body "Overview of available rooms"
    smoke_report
    echo 'Smoke Tests Successfully Completed.'
    break
  elif [[ $TIME_OUT_COUNT -gt $TIME_OUT ]]; then
    echo "Process has Timed out! Elapsed Timeout Count.. $TIME_OUT_COUNT"
    exit 1
  else
    echo "Checking Status on host $SMOKE... $TIME_OUT_COUNT seconds elapsed"
    TIME_OUT_COUNT=$((TIME_OUT_COUNT+10))
  fi
  sleep 10
done
