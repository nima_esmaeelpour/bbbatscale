#!/usr/bin/env sh
set -e

if [ $# = 0 ]; then
  exec bbbatscale run --migrate
else
  exec "$@"
fi
